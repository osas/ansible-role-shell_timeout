Ansible role to enforce a policy of shell timeout

This will disconnect any session after 300 seconds by default,
for bash shell.
